var mdns                    = require('mdns');
var mManager                = require('./MediaManager/manager');
var webUi                   = require('./MediaManager/interface');
var cc                      = require('./MediaManager/playbackhandler');

var running = false;
var browser = mdns.createBrowser(mdns.tcp('googlecast'));

browser.on('serviceUp', function(service) {
  console.log('found device "%s" at %s:%d', service.name, service.addresses[0], service.port);
  //ondeviceup(service.addresses[0]);
  cc.init(service.addresses[0]);
  browser.stop();
});

if(!running){
    browser.start();
    mManager.enumerateMedia(webUi.routeInit);
    running = true;
    webUi.init();
}