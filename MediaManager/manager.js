var settings = require('../settings.json');
var readdirp = require('readdirp');

var _videoTypes = [
    "*.avi",
    "*.mkv",
    "*.mp4",
    "*.flv"
];

var _audioTypes = [
    "*.mp3",
    "*.ogg",
    "*.wma",
    "*.flac"
];

var collections = [];
var collected = 0;
var requestedCollections = 0;

function getMediaCollection(){
    console.log("[GETMEDIA]");
    console.log("[GM] # of collections: "+collections.length);
    return collections;
    
    
}

function parseMediaEntry(entry, typesArr) {
    console.log("Parsing media...");
    var collectionInfo = {};
    collectionInfo.name = entry.name;
    collectionInfo.type = entry.type;
    collectionInfo.files = [];
    console.log("[MEDIA]: path-arr length:  " + entry.paths.length);
    for (var pi = 0; pi < entry.paths.length; pi++) {
        console.log("[MEDIA]: path [IDX=" + pi + "]:  " + entry.paths[pi]);
        readdirp({ root: entry.paths[pi], fileFilter: typesArr, directoryFilter: ['!.git', '!*modules'] }).on('data', function (ent) {

            var mediaObj = {
                filename: ent.name,
                path: ent.fullPath
            };
            //console.log("[MEDIAOBJ]: " + ent.name);
            collectionInfo.files.push(mediaObj);
        });/*, function(err, res){
            if(err && err.length > 0){
                for(var ei = 0; ei < err.length; ei++){
                    	console.log("{ERR}: [rdp-"+ei+"]: "+err[ei] + "");
                }
            }
            
        }); */
        
    }
    collections.push(collectionInfo);
    collected++;

}

function parseMiscMediaInfo(entry) {
    // NYI
    console.log("[NYI] - parseMisc: " + entry.name);
}
function getMedia(callback) {

    var mediaEntries = settings.media;
    requestedCollections = mediaEntries.length;
    for (var mediaIdx = 0; mediaIdx < mediaEntries.length; mediaIdx++) {
        switch (mediaEntries[mediaIdx].type) {
            case "video":
                parseMediaEntry(mediaEntries[mediaIdx], _videoTypes);
                break;
            case "audio":
                parseMediaEntry(mediaEntries[mediaIdx], _audioTypes);
                break;
            default:
                parseMiscMediaInfo(mediaEntries[mediaIdx]);
                break;
        }
    }

    callback();
}

module.exports = {
    enumerateMedia: getMedia,
    getCollections: getMediaCollection
}