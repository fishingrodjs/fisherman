var express = require('express');
var mediaMan = require('./manager');
var playback = require('./playbackhandler');

var mediaCollections = mediaMan.getCollections();

var app = express();

var PORT = 8976;



function setupMainRoutes() {
    app.get('/', function (req, res) {
        //console.log("/-route");
        mediaCollections = mediaMan.getCollections();
        res.render('list', { mediaCollections: mediaMan.getCollections(), status: null });
    });
    app.get('/media/:collection/:fileid', function (req, res) {
        //var requestUrl = 
        var reqUrl = req.protocol + '://' + req.get('host') + req.originalUrl;
        var mediaFile = mediaCollections[req.params.collection].files[req.params.fileid];
        var mediaType = mediaCollections[req.params.collection].type;
        playback.startCasting(mediaFile, mediaType, res, reqUrl);
    });
    app.get('/media/:collection/:fileid/play', function (req, res) {
        var mediaPath = mediaCollections[req.params.collection].files[req.params.fileid].path;
        var type = mediaCollections[req.params.collection].type;
        playback.transcode(mediaPath, type, res);
    });
    console.log("--ROUTES UP--");
}


app.use(express.static('./web/static'));
app.set('views', './web');
app.set('view engine', 'ejs');

function webUp() {

    console.log("Web UI up (port: " + PORT + ")");
    app.listen(PORT);
    mediaCollections =  mediaMan.getCollections();
}

module.exports = {
    init: webUp,
    routeInit: setupMainRoutes
}