var ffmpeg = require('fluent-ffmpeg');
var CCast = require('castv2-client').Client;
var DefaultMediaReceiver = require('castv2-client').DefaultMediaReceiver;
var client = new CCast();
var mediaMan = require('./manager');
var clientAddress;
var currPlaying = null;

function initChromeCast(address) {
    clientAddress = address;
}

function videoPreset(command) {
    command.format("mp4").videoCodec('libx264').outputOption('-movflags frag_keyframe+faststart');
}

function audioPreset(command) {
    command.format("mp3").noVideo();
}

function startCast(file, type, response, reqUrl) {
    if(currPlaying != null){
        currPlaying.stop();
        currPlaying = null;
    }
    response.render('list', { mediaCollections: mediaMan.getCollections(), status: "Playback started." });


    client.connect(clientAddress, function () {
        console.log('connected, launching app ...');

        client.launch(DefaultMediaReceiver, function (err, player) {
            currPlaying = player;
            var media = {

                // Here you can plug an URL to any mp4, webm, mp3 or jpg file with the proper contentType.
                contentId: reqUrl + '/play',
                contentType: type == 'video' ? 'video/mp4' : 'audio/mp3',
                streamType: 'LIVE', // or LIVE

                // Title and cover displayed while buffering
                metadata: {
                    type: 0,
                    metadataType: 0,
                    title: file.name,
                    images: [
                        {
                            url: '' //TBA
                        }
                    ]
                }
            };

            player.on('status', function (status) {
                console.log('status broadcast playerState=%s', status.playerState);
            });

            console.log('app "%s" launched, loading media %s ...', player.session.displayName, media.contentId);

            player.load(media, { autoplay: true }, function (err, status) {
                //console.log('err? %s', err);
                //console.log('media loaded playerState=%s', status.playerState);


            });

        });

    });

    client.on('error', function (err) {
        console.log('Error: %s', err.message);
        client.close();
    });


}

function transcode(path, type, res) {
    console.log(path);
    var trans = ffmpeg(path);
    if (type == "video") {
        res.contentType('mp4');
        trans.preset(videoPreset).on('error', function (err, stdout, stderr) {
            console.log('Error: ' + err.message);
            console.log('ffmpeg output:\n' + stdout);
            console.log('ffmpeg stderr:\n' + stderr);
        }).pipe(res, { end: true });
    } else {
        res.contentType('mp3');
        trans.preset(audioPreset).pipe(res, { end: true });
    }
}

module.exports = {
    startCasting: startCast,
    init: initChromeCast,
    transcode: transcode
}